package com.epam.tasks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

public class Synchronizer {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final Object sync1 = new Object();
    private static final Object sync2 = new Object();
    private static final Object sync3 = new Object();
    private static final int SLEEP_TIME = 3;

    private void method1(Object sync) throws InterruptedException {
        synchronized (sync) {
            LOGGER.info(LocalDateTime.now() + "\n");
            LOGGER.info("mehod1 is operating with object\n");
            TimeUnit.SECONDS.sleep(SLEEP_TIME);
            LOGGER.info(LocalDateTime.now() + "\n");
        }
    }

    private void method2(Object sync) throws InterruptedException {
        synchronized (sync) {
            LOGGER.info(LocalDateTime.now() + "\n");
            LOGGER.info("mehod2 is operating with object\n");
            TimeUnit.SECONDS.sleep(SLEEP_TIME);
            LOGGER.info(LocalDateTime.now() + "\n");
        }
    }

    private void method3(Object sync) throws InterruptedException {
        synchronized (sync) {
            LOGGER.info(LocalDateTime.now() + "\n");
            LOGGER.info("mehod3 is operating with object\n");
            TimeUnit.SECONDS.sleep(SLEEP_TIME);
            LOGGER.info(LocalDateTime.now() + "\n");
        }
    }

    public void demonstrate() {
        new Thread(() -> {
            try {
                method1(sync1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                method2(sync2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                method3(sync3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
