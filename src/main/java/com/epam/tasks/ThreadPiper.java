package com.epam.tasks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Scanner;

public class ThreadPiper {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Scanner SCANNER = new Scanner(System.in);
    private static final Object sync = new Object();
    private PipedInputStream inputStream;
    private PipedOutputStream outputStream;

    public ThreadPiper() {
        inputStream = new PipedInputStream();
        outputStream = new PipedOutputStream();
        try {
            inputStream.connect(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendMessage() throws IOException {
        LOGGER.info(Thread.currentThread().getName() + ": ");
        String message = SCANNER.nextLine();
        LOGGER.info(Thread.currentThread().getName() + " is sending message...\n");
        outputStream.write((Thread.currentThread().getName() + ": " + message).getBytes());
        if (message.equals("exit")) {
            Thread.currentThread().interrupt();
        }
    }

    private void readMessage() throws IOException {
        byte[] buffer = new byte[inputStream.available()];
        inputStream.read(buffer);
        String message = new String(buffer);
        if (!message.isEmpty()) {
            LOGGER.info(Thread.currentThread().getName() + " has received a message!\n");
            LOGGER.info(message + "\n");
        }
        if (message.matches(".*exit")) {
            Thread.currentThread().interrupt();
        }
    }

    private void communicateFirst() throws IOException, InterruptedException {
        synchronized (sync) {
            while (!Thread.interrupted()) {
                sendMessage();
                sync.wait();
                sync.notify();
                readMessage();
                if (Thread.interrupted()) {
                    inputStream.close();
                    outputStream.close();
                    break;
                }
            }
        }
    }

    private void communicateSecond() throws IOException, InterruptedException {
        synchronized (sync) {
            while (!Thread.interrupted()) {
                readMessage();
                sendMessage();
                sync.notify();
                sync.wait();
                if (Thread.interrupted()) {
                    inputStream.close();
                    outputStream.close();
                    break;
                }
            }
        }
    }

    public void startCommunication() {
        Thread user1 = new Thread(() -> {
            try {
                communicateFirst();
            } catch (IOException | InterruptedException e) {
                LOGGER.info(e.getMessage());
            }
        }, "User_1");
        Thread user2 = new Thread(() -> {
            try {
                communicateSecond();
            } catch (IOException | InterruptedException e) {
                LOGGER.info(e.getMessage());
            }
        }, "User_2");
        user1.start();
        user2.start();
    }
}
