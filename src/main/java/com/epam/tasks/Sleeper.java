package com.epam.tasks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Sleeper implements Runnable {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final Random RANDOM = new Random();
    private static final int MAX_SLEEP_TIME = 10;
    private int executionNumber;

    public Sleeper(int executionNumber) {
        this.executionNumber = executionNumber;
    }


    public void start() {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(executionNumber);
        for (int i = 0; i < executionNumber; i++) {
            executor.schedule(this, 0, TimeUnit.SECONDS);
        }
        executor.shutdown();
    }

    @Override
    public void run() {
        int sleepTime = RANDOM.nextInt(MAX_SLEEP_TIME) + 1;
        try {
            TimeUnit.SECONDS.sleep(sleepTime);
        } catch (InterruptedException e) {
            LOGGER.info(e.getMessage());
        }
        LOGGER.info(Thread.currentThread().getName() + " is running after " + sleepTime + " seconds of sleeping\n");
    }
}
