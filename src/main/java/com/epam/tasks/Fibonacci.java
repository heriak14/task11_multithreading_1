package com.epam.tasks;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Fibonacci implements Runnable {

    private int number;
    private int repeatNumber;

    public Fibonacci(int number, int repeat) {
        this.number = number;
        this.repeatNumber = repeat;
    }

    @Override
    public void run() {
        Set<Integer> sequence = new LinkedHashSet<>();
        buildFibonacci(number, sequence);
        System.out.print(sequence + " ");
    }

    public void start() {
        for (int i = 0; i < repeatNumber; i++) {
            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.submit(this);
            try {
                System.out.println(executor.submit(this::getSum).get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
            executor.shutdown();
        }
    }

    private int getSum() {
        Set<Integer> sequence = new LinkedHashSet<>();
        buildFibonacci(number, sequence);
        return sequence.stream()
                .reduce(Integer::sum)
                .orElse(0);
    }

    private int buildFibonacci(int n, Set<Integer> sequence) {
        if (n == 1) {
            sequence.add(n);
            return n;
        } else if (n == 2) {
            sequence.add(n);
            return n;
        }
        int next = buildFibonacci(n - 1, sequence) + buildFibonacci(n - 2, sequence);
        sequence.add(next);
        return next;
    }
}
