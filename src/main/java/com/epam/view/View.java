package com.epam.view;

import com.epam.tasks.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    private static final Logger LOG = LogManager.getLogger();
    private static final Scanner SCAN = new Scanner(System.in, "UTF-8");
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public View() {
        menu = new LinkedHashMap<>();
        menu.put("1", "Play ping-pong");
        menu.put("2", "Print fibonacci");
        menu.put("3", "Test thread service pool");
        menu.put("4", "Demonstrate work of synchronized keyword");
        menu.put("5", "Start messenger between two threads");
        menu.put("Q", "QUIT");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", PingPong::startGame);
        methodsMenu.put("2", this::printFibonacci);
        methodsMenu.put("3", this::testThreadPool);
        methodsMenu.put("4", new Synchronizer()::demonstrate);
        methodsMenu.put("5", new ThreadPiper()::startCommunication);
        methodsMenu.put("Q", this::quit);
    }

    private void showMenu(Map<String, String> menu) {
        LOG.trace("------------MENU------------\n");
        menu.forEach((k, v) -> LOG.info(k + " - " + v + "\n"));
    }

    private void printFibonacci() {
        LOG.info("Enter number of numbers: ");
        String input = SCAN.nextLine();
        if (!input.matches("\\d")) {
            return;
        }
        int number = Integer.parseInt(input);
        LOG.info("Enter number of repetitions: ");
        input = SCAN.nextLine();
        if (!input.matches("\\d")) {
            return;
        }
        int repeat = Integer.parseInt(input);
        Fibonacci fibonacci = new Fibonacci(number, repeat);
        fibonacci.start();
    }

    private void testThreadPool() {
        LOG.info("Enter number of executions: ");
        String input = SCAN.nextLine();
        if (!input.matches("\\d")) {
            return;
        }
        int number = Integer.parseInt(input);
        new Sleeper(number).start();
    }

    private void quit() {
    }

    public void show() throws InterruptedException {
        String key;
        do {
            showMenu(menu);
            LOG.trace("Enter your choice: ");
            key = SCAN.nextLine().toLowerCase();
            if (methodsMenu.containsKey(key)) {
                methodsMenu.get(key).print();
                Thread.currentThread().join();
            } else if (!key.equals("q")) {
                LOG.trace("Wrong input!\n");
            }
        } while (!key.equals("q"));
    }
}
