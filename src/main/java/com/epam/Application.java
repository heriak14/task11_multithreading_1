package com.epam;

import com.epam.tasks.*;
import com.epam.view.View;

public class Application {
    public static void main(String[] args) {
        try {
            new View().show();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
