package com.epam.tasks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class PingPong {
    private static final Logger LOG = LogManager.getLogger();
    private static final Random RANDOM = new Random();
    private static final Object lock = new Object();
    private static final int MAX_CHANCE = 100;
    private static int hitProbability = 80;
    private static Thread player1;
    private static Thread player2;

    public static void startGame() {
        player1 = new Thread(() -> {
            try {
                playPing();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        player2 = new Thread(() -> {
            try {
                playPong();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        player1.start();
        player2.start();
    }

    private static void playPing() throws InterruptedException {
        synchronized (lock) {
            while (!Thread.currentThread().isInterrupted()) {
                if (RANDOM.nextInt(MAX_CHANCE) < hitProbability) {
                    LOG.info("ping\n");
                    lock.wait();
                    lock.notify();
                } else {
                    Thread.currentThread().interrupt();
                    hitProbability = 0;
                    lock.notify();
                    break;
                }
            }
        }
    }
    private static void playPong() throws InterruptedException {
        synchronized (lock) {
            while (!Thread.currentThread().isInterrupted()) {
                if (RANDOM.nextInt(MAX_CHANCE) < hitProbability) {
                    LOG.info("pong\n");
                    lock.notify();
                    lock.wait();
                } else {
                    Thread.currentThread().interrupt();
                    hitProbability = 0;
                    lock.notify();
                    break;
                }
            }
        }
    }
}
